<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Validator;
use Carbon\Carbon;

use App\User;
use App\Post;

class ApiController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function getProfile(Request $request)
    {
        $user = Auth::user();
        if(empty($user)) {
            return response()->json([
                'status' => 'error',
                'message' => 'User not found!'
            ]);
        }
        return response()->json([
            'status' => 'success',
            'data' => $user
        ]);
    }

    public function updateProfile(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'firstName'     => 'required|string|max:255',
            'lastName'      => 'required|string|max:255',
            'email'         => 'required|string|email|max:255|unique:users,email,' . $user->id,
            'phone'         => 'sometimes|string',
            'profilePic'    => 'sometimes|image|nullable|mimes:jpeg,jpg,png|max:156360',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ]);
        }

        $user->email = $request->email;
        $user->firstName = $request->firstName;
        $user->lastName = $request->lastName;
        $user->phone = $request->phone;
        $user->save();

        return response()->json([
            'status' => 'success',
            'data'   => $user,
            'message' => 'Profile updated successfully.'
        ]);
    }

    public function getPosts(Request $request)
    {
        $user = Auth::user();
        $posts = Post::all();

        foreach ($posts as $post) {
            $post->categories = implode(', ', json_decode($post->categories, true));
        }

        return response()->json([
            'status' => 'success',
            'data'   => $posts
        ]);
    }

    public function getPost($id)
    {
        if(!isset($id) || empty($id)) {
            return response()->json([
                'status' => 'failed',
            ]);
        }

        $post = Post::find($id);
        $post->categories = json_decode($post->categories, true);

        return response()->json([
            'status' => 'success',
            'data'   => $post
        ]);
    }

    public function createEditPost(Request $request, $id = NULL)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'title'             => 'required|string|max:255',
            'slug'              => 'required|string|max:255|unique:posts,slug',
            'datePublished'     => 'required|date',
            'categories'        => 'required|array',
            'visible'           => 'required|boolean',
            'content'           => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ]);
        }

        if(isset($id) && !empty($id)) $post = Post::findOrFail($id);
        else $post = new Post;

        $post->title            = $request->title;
        $post->slug             = $request->slug;
        $post->date_published   = Carbon::parse($request->date_published)->format('Y-m-d');
        $post->categories       = json_encode($request->categories);
        $post->visible          = $request->visible;
        $post->content          = $request->content;
        $post->save();

        return response()->json([
            'status' => 'success',
            // 'data'   => $post,
            'action' => $post->wasRecentlyCreated ? 'created' : 'updated'
        ]);
    }
}