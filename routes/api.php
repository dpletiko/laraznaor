<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::group(['middleware' => ['cors']], function () {
	Route::post('auth/login', 'AuthController@login');

	Route::group(['middleware' => 'jwt.refresh'], function(){
	  Route::get('auth/refresh', 'AuthController@refresh');
	});

	// Route::group(['middleware' => ['auth:api']], function () {
	Route::group(['middleware' => ['jwt.auth']], function () {
	  Route::get('auth/user', 'AuthController@user');
	  Route::post('auth/logout', 'AuthController@logout');

		Route::get('/profile', 'ApiController@getProfile');
		Route::post('/profile', 'ApiController@updateProfile');

		Route::get('/get-posts', 'ApiController@getPosts');

		Route::get('/blog/{id}', 'ApiController@getPost');
		Route::post('/blog/new', 'ApiController@createEditPost');
		Route::post('/blog/{id}', 'ApiController@createEditPost');

	});
// });