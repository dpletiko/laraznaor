<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(array(
            'firstName'     => 'Iva',
            'lastName'      => 'Znaor',
            'email'         => 'admin@test.com',
            'phone'         => '',
            'profilePic'    => null,
            'password'      => bcrypt('admin')
        ));
    }
}
