import Vue from 'vue'

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import VueAuth from '@websanova/vue-auth'
import VueResource from 'vue-resource'
Vue.use(VueResource)

// Vue.http.headers.common['X-CSRF-TOKEN'] = document.head.querySelector('meta[name="csrf-token"]').content;
Vue.http.options.root = 'http://localhost:92/api'

import BootstrapVue from 'bootstrap-vue'
import VueMaterial from 'vue-material'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-material/dist/vue-material.min.css'
// import 'vue-material/dist/theme/default.css'
// import '../../../public/css/app.css'
Vue.use(BootstrapVue);
Vue.use(VueMaterial)
// Vue.material.locale.dateFormat = 'DD.MM.YYYY'

import moment from 'moment'
import 'moment/locale/hr'
moment.locale('hr');
Vue.prototype.$moment = moment;

import VueSweetalert2 from 'vue-sweetalert2'
Vue.use(VueSweetalert2);
import VueMoment from 'vue-moment'
Vue.use(VueMoment);

import App from './App'

import Login from './components/Auth/Login'

import Dashboard from './components/Administration/Dashboard'
import UserProfile from './components/Administration/UserProfile'
import Blog from './components/Administration/Blog'
import Post from './components/Administration/Blog/Post'
// import Gallery from './components/Gallery'
import NotFound from './components/GeneralViews/NotFound.vue'

const router = new VueRouter({
  mode: 'history',
	routes: [{
	//   path: '/',
	//   name: 'home',
	//   component: NotFound
	// }, {
	  path: '/login',
	  name: 'login',
	  component: Login,
	  meta: {
	  	title:'Login',
	  	auth: false
	  }
	}, {
	  path: '/administration',
	  name: 'administration',
	  component: Dashboard,
	  children: [{
		  path: '',
		  name: 'administration-dashboard',
		  component: NotFound,
		  meta: {
		  	title: 'Dashboard',
		  }
		}, {
		  path: 'profile',
		  name: 'administration-profile',
		  component: UserProfile,
		  meta: {
		  	title: 'Profile',
		  }
		}, {
		  path: 'blog',
		  name: 'administration-blog',
		  component: Blog,
		  meta: {
		  	title: 'Blog',
		  },
		},{
	  	path: 'blog/new',
	  	name: 'administration-blog-new',
	  	component: Post,
	  	meta: {
	  		title: 'New post',
	  	}
	  }, {
	  	path: 'blog/:id',
	  	name: 'administration-blog-edit',
	  	component: Post,
	  	meta: {
	  		title: 'Edit post',
	  	}
		}],
	  meta: {
	  	auth: true
	  }
	}, { 
		path: '*', 
		redirect: '/login',
		// component: NotFound 
	}],
	linkActiveClass: 'active'
});

Vue.router = router

Vue.use(VueAuth, {
  auth: require('@websanova/vue-auth/drivers/auth/bearer.js'),
  http: require('@websanova/vue-auth/drivers/http/vue-resource.1.x.js'),
  router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
	loginData: { fetchUser: true },
  fetchData: { enabled: true },
  parseUserData: function (response) {
  	Vue.auth.user(response.data)
  	return response.data
  }
});

const app = new Vue({
  el: '#app',
	components: { App },
	template: '<App/>',
  router,
});